//
//  MainScreenViewController.swift
//  contactisTest
//
//  Created by Artem Alekseev on 01/08/2017.
//  Copyright © 2017 Artem Alekseev. All rights reserved.
//

import UIKit
import Speech

class MainScreenViewController: UIViewController, SFSpeechRecognizerDelegate {

    //MARK: Outlets
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var buttonLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var processingLabel: UILabel!
    
    //MARK: Properties
    private let audioEngine = AVAudioEngine()
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    private var inputNode: AVAudioInputNode?
    private var logic: BusinessLogic?
    private var isRecording = false
    
    //MARK: UIViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
    }
    
    //MARK: Methods
    
    func setupViewController() {
        speechRecognizer.delegate = self
        processingLabel.isHidden = true
        inputNode = audioEngine.inputNode
        logic = BusinessLogic()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    func proceedRecording() {
        SFSpeechRecognizer.requestAuthorization { (authorizationStatus) in
            DispatchQueue.main.async {
                switch authorizationStatus {
                case .authorized:
                    do {
                        try self.startRecording()
                    } catch let error {
                        print("Error while recording = \(error)")
                    }
                case .denied:
                    print("Authorization denied")
                case .notDetermined:
                    print("Authorization not yet determined")
                case .restricted:
                    print("Recognition is not available on this device")
                }
            }
        }
    }
    
    private func startRecording() throws {
        
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        processingLabel.isHidden = false
        processingLabel.text = "Listening..."
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest!) {
            [unowned self]
            (result, _) in
            if let transcription = result?.bestTranscription {
                //Magic starts here
                self.resultLabel.text = self.logic?.calculateResult(inputString: transcription.formattedString)
            }
        }
        
        let recordingFormat = inputNode?.outputFormat(forBus: 0)
        inputNode?.installTap(onBus: 0, bufferSize: 1024,
                        format: recordingFormat) { [unowned self]
                            (buffer, _) in
                            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        try audioEngine.start()
    }
    
    private func stopRecording() {
        inputNode?.reset()
        inputNode?.removeTap(onBus: 0)
        audioEngine.stop()
        recognitionRequest?.endAudio()
        recordButton.isEnabled = true
        processingLabel.isHidden = true
        buttonLabel.text = "Tap to start recording"
    }

    //MARK: SFSpeechRecognizerDelegate
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            recordButton.isEnabled = true
            processingLabel.isHidden = true
            buttonLabel.text = "Tap to start recording"
        } else {
            try! startRecording()
            recordButton.isEnabled = false
            buttonLabel.text = "Something goes wrong"
        }
    }
    
    //MARK: UIButton Actions
    
    @IBAction func recordButtonDidTapped(_ sender: UIButton) {
        if audioEngine.isRunning {
            recordButton.isEnabled = false
            processingLabel.isHidden = false
            processingLabel.text = "Stopping..."
            stopRecording()
        } else {
            buttonLabel.text = "Tap to stop"
            proceedRecording()
        }
    }
    
}

