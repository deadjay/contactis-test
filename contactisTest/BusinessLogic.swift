//
//  BusinessLogic.swift
//  contactisTest
//
//  Created by Artem Alekseev on 02/08/2017.
//  Copyright © 2017 Artem Alekseev. All rights reserved.
//

import UIKit
import Speech

class BusinessLogic {
    
    var resultString = ""
    
    //MARK: Methods
    
    func calculateResult(inputString: String) -> String {
        
        resultString = inputString
        checkSentence(string: inputString)
        return resultString
    }
    
    private func checkSentence(string: String) {
        let set = NSCharacterSet.decimalDigits
        //checking if string contains some numbers
        if string.rangeOfCharacter(from: set) != nil {
            //checking if there's at least one of the math operations
            if (string.range(of: "+") != nil ||
                string.range(of: "-") != nil ||
                string.range(of: "×") != nil ||
                string.range(of: "÷") != nil) {
                convertSentence(string: string)
            }
        }
    }
    
    private func convertSentence(string: String) {
        //replacing this operations, because NSExpression understands only this ones
        var rightString = string.replacingOccurrences(of:"×", with: "*")
        rightString = rightString.replacingOccurrences(of:"÷", with: "/")
        //replacing "One " with "1", because speech recognizer always represents this number as a word
        rightString = rightString.replacingOccurrences(of:"One ", with: "1")
        
        //let components = rightString.components(separatedBy:"")
        //let newString = components.joined(separator:"")
        //print("Components = \(newString)")
        
        var customSet = NSCharacterSet.decimalDigits
        customSet.insert(charactersIn:"+-*/")
        
        //checking if our string contains nothing but decimalDigits and math expression symbols
        if rightString.rangeOfCharacter(from: customSet.inverted) == nil {
            executeOperation(completeString: rightString)
        }
    }

    private func executeOperation(completeString: String) {
        let decimalSet = NSCharacterSet.decimalDigits
        let firstCharacter = completeString[completeString.startIndex]
        let lastCharacter = completeString[completeString.index(before: completeString.endIndex)]
        
        //checking if our string starts and ends with digits only,
        //because of this we're sure, that our expression is written right and can be executed by NSExpression in future
        if firstCharacter.description.rangeOfCharacter(from: decimalSet) != nil &&
            lastCharacter.description.rangeOfCharacter(from: decimalSet) != nil {
            let mathExpression = NSExpression(format:completeString)
            if let mathValue = mathExpression.expressionValue(with: nil, context: nil) as? Int {
                //executing math expression
                formatResultNumber(resultNumber: mathValue)
            }
        }
    }
    
    private func formatResultNumber(resultNumber: Int) {
        //if all steps of calculation is done, then we're formatting result number into a word to show to the user
        let numberValue = NSNumber(integerLiteral: resultNumber)
        let formatter = NumberFormatter()
        formatter.numberStyle = .spellOut
        if let formattedString = formatter.string(from: numberValue) {
            resultString = formattedString
        }
    }
    
}
